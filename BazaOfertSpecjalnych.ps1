﻿function Out-Log {
    Param(
        [Parameter(Mandatory = $True)][ValidateNotNullOrEmpty()]$Message,
        [Parameter(Mandatory = $True)]$Config
    )
    Out-File -Append -FilePath "$($Config.LogsFolder)\$((Get-Date).ToString('yyyy-MM-dd')).log" -InputObject "$((Get-Date).ToString('yyyy-MM-dd HH:mm:ss:ms')): $Message" -Encoding $($Config.LogEncoding)
}
function Get-Config {
    Param(
        [ValidateScript( {Test-Path $PSItem -PathType ‘Leaf’})][string]$FileName 
    )
    $Config = [xml](Get-Content -Path $FileName)
    return $Config.Config
}
function Set-EWSService {
    param(
        $Config
    )
    $Service = New-Object Microsoft.Exchange.WebServices.Data.ExchangeService($Config.EWSRequestedServerVersion)
    $Service.TraceEnabled = $false
    try {$Service.Credentials = New-Object System.Net.NetworkCredential($Config.AccountName, ($Config.SecurePassword | ConvertTo-SecureString -ErrorAction Stop), $Config.Domain) -ErrorAction Stop}
    catch {throw $PSItem}
    $Service.Url = $Config.EWSURL

    return $Service
}
function Invoke-IgnoreSSLWarningIssues {
    $Provider = New-Object Microsoft.CSharp.CSharpCodeProvider
    $Compiler = $Provider.CreateCompiler()
    $Params = New-Object System.CodeDom.Compiler.CompilerParameters
    $Params.GenerateExecutable = $False
    $Params.GenerateInMemory = $True
    $Params.IncludeDebugInformation = $False
    $Params.ReferencedAssemblies.Add("System.DLL") | Out-Null

    $TASource = @'
  namespace Local.ToolkitExtensions.Net.CertificatePolicy{
    public class TrustAll : System.Net.ICertificatePolicy {
      public TrustAll() { 
      }
      public bool CheckValidationResult(System.Net.ServicePoint sp,
        System.Security.Cryptography.X509Certificates.X509Certificate cert, 
        System.Net.WebRequest req, int problem) {
        return true;
      }
    }
  }
'@ 
    $TAResults = $Provider.CompileAssemblyFromSource($Params, $TASource)
    $TAAssembly = $TAResults.CompiledAssembly
    $TrustAll = $TAAssembly.CreateInstance("Local.ToolkitExtensions.Net.CertificatePolicy.TrustAll")
    [System.Net.ServicePointManager]::CertificatePolicy = $TrustAll
}
function Start-BazaOfertSpecjalnych {
    Param(
        [parameter()]$Config,
        [parameter()]$Service
    )
    $Mailbox = New-Object Microsoft.Exchange.WebServices.Data.Mailbox($Config.MailboxName)
    $RootFolderName = [Microsoft.Exchange.WebServices.Data.WellKnownFolderName]::($Config.FolderName)
    $RootFolderId = new-object Microsoft.Exchange.WebServices.Data.FolderId($RootFolderName, $Mailbox)
    $RootFolder = [Microsoft.Exchange.WebServices.Data.Folder]::Bind($Service, $RootFolderId)
    $ItemView = new-object Microsoft.Exchange.WebServices.Data.ItemView(100)
    $IfIsRead = new-object Microsoft.Exchange.WebServices.Data.SearchFilter+IsEqualTo([Microsoft.Exchange.WebServices.Data.EmailMessageSchema]::IsRead, $False)
    $ProcessedFolder = $Service.FindFolders([Microsoft.Exchange.Webservices.Data.WellKnownFolderName]::MsgFolderRoot, (New-Object Microsoft.Exchange.WebServices.Data.FolderView(100))) | Where-Object Displayname -eq $Config.ProcessedEmailsFolder

    do {
        $Messages = @($RootFolder.FindItems($IfIsRead, $ItemView)) | Sort-Object DateTimeReceived
        if ($Messages.Count -ne 0) {
            Out-Log -Message "$($Messages.Count) new message(s)" -Config $Config
            $Messages.Load() | Out-Null
            foreach ($Message in $Messages) {
                $OutputData = New-Object psobject
                Add-Member -InputObject $OutputData -MemberType NoteProperty -Name DateTimeReceived -Value $Message.DateTimeReceived.ToString("G")
                Add-Member -InputObject $OutputData -MemberType NoteProperty -Name From -Value $Message.From.Address
                Add-Member -InputObject $OutputData -MemberType NoteProperty -Name ToRecipients -Value ($Message.ToRecipients.Address -join ", ")
                Add-Member -InputObject $OutputData -MemberType NoteProperty -Name CCRecipients -Value ($Message.CCRecipients.Address -join ", ")
                Add-Member -InputObject $OutputData -MemberType NoteProperty -Name Subject -Value ($Message.Subject -replace ":", "")

                if (Test-Path "$($Config.OutputPath)\$($Message.DateTimeCreated.ToShortDateString())_$($OutputData.Subject)" -ErrorAction SilentlyContinue) {$Directory = mkdir -Path "$($Config.OutputPath)\$(($OutputData.DateTimeReceived -replace " ", "_") -replace ":", ".")_$($OutputData.Subject)"}
                else {$Directory = mkdir -Path "$($Config.OutputPath)\$($Message.DateTimeCreated.ToShortDateString())_$($OutputData.Subject)"}
                
                foreach ($Attachment in $Message.Attachments) {
                    $Attachment.Load() | Out-Null
                    $OutputFile = New-Object System.IO.FileStream("$($Directory.FullName)\$($Attachment.Name)", [System.IO.FileMode]::Create)
                    $OutputFile.Write($Attachment.Content, 0, $Attachment.Content.Length)
                    $OutputFile.Close()
                }

                $PropertySet = New-Object Microsoft.Exchange.WebServices.Data.PropertySet([Microsoft.Exchange.WebServices.Data.ItemSchema]::MimeContent)   
                $Message.Load($PropertySet) | Out-Null
                $OutputFile = New-Object System.IO.FileStream("$($Directory.FullName)\$($OutputData.Subject).eml", [System.IO.FileMode]::Create) 
                $OutputFile.Write($Message.MimeContent.Content, 0, $Message.MimeContent.Content.Length)
                $OutputFile.Close()

                Add-Member -InputObject $OutputData -MemberType NoteProperty -Name ProjectPath -Value ($Directory.FullName)
                $Message.Move($ProcessedFolder.id) | Out-Null
                Export-Csv -InputObject $OutputData -NoTypeInformation -Encoding $Config.OutputFileEncoding -Append -Path $Config.OutputFile
            }
        }
        else {Out-Log -Message "No new messages" -Config $Config}
        if ($Config.ServiceMode -eq 'True') {Start-Sleep -Seconds ($Config.CheckInterval)}
    } while ($Config.ServiceMode -eq 'True')
}

$Config = Get-Config -FileName C:\BazaOfertSpecjalnych\BazaOfertSpecjalnychConfig.xml
if ($Config.DebugMode -eq 'True') {Start-Transcript -Path "$($Config.LogsFolder)\Transcript_$((Get-Date).ToString('yyyy-MM-dd')).log" -Append}
[void][Reflection.Assembly]::LoadFile($Config."Microsoft.Exchange.WebServices.dllPath")
$EWSService = Set-EWSService -Config $Config -ErrorAction Stop
if ($Config.IgnoreSSLWarningIssues -eq 'True') {Invoke-IgnoreSSLWarningIssues}
Start-BazaOfertSpecjalnych -Config $Config -Service $EWSService
if ($Config.DebugMode -eq 'True') {Stop-Transcript}